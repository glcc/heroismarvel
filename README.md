
App from a tutorial by Eric Brito

Teaches how to install and use CocoaPods 

Pods installed
    
    Alamofire
    Kingfisher
    SwiftHash


![LaunchScreen](/Screens/Screen01.png)  ![WelcomeScreen](/Screens/Screen02.png)   ![SearchingScreen](/Screens/Screen03.png)   ![HeroListScreen](/Screens/Screen04.png)   ![HeroScreen](/Screens/Screen05.png)

