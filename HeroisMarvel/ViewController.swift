//
//  ViewController.swift
//  HeroisMarvel
//
//  Created by Gerson Costa on 23/12/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tfName: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! HeroesTableViewController
        vc.name = tfName.text
        tfName.resignFirstResponder()
    }
}


